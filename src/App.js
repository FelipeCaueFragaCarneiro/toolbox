import './App.css';
import MiniDrawer from './components/menu/menu';
import * as React from "react";
import {Route, Switch} from "react-router-dom";
import ConnectForm from "./components/instanceform";
import ExportData from "./components/exportData";
import BulkCustomField from "./components/bulkCF";

function App() {
  return (
    <div className="App">
            <MiniDrawer />
            <Switch>
                <Route
                    exact from="/"
                    render={props => <ConnectForm  {...props} />}
                />
                <Route
                    exact path="/export"
                    render={props => <ExportData  {...props}/>}
                />
                <Route
                    exact path="/bulk"
                    render={props => <BulkCustomField {...props} />}
                />
            </Switch>
    </div>
  );
}

export default App;
