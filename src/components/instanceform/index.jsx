import React,{ useState } from "react";
import { useForm } from "react-hook-form";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import {createInstance} from "../../engine/dbConnect";
import {FormControl} from "@material-ui/core";
import {
    Divider,
    TableContainer,
    Table,
    TableCell,
    TableHead,
    TableRow,
    paper,
    TableBody
} from "@mui/material";

function ConnectForm(){
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);
    const rows = [
        {host: "https://contractsstag.atlassian.net", user: "felipecaue.fraga@gmail.com"},
        {host:"https://mindpro.atlassian.net/",user: "felipe.carneiro@e-core.com"}

    ];
    return(
        <>
                <FormControl variant="outlined" onSubmit={handleSubmit(onSubmit)} >
                    {/*<form onSubmit={handleSubmit(onSubmit)}>*/}
                    {/* register your input into the hook by invoking the "register" function */}
                    <TextField
                        defaultValue="test"
                        {...register("example")}
                        label="host name"
                        type={"url"}
                        helperText="Infrome hostname from Jira."
                    />

                    {/* include validation with required or other standard HTML validation rules */}
                    <TextField
                        {...register("exampleRequired",
                            { required: true })}
                        label="User E-mail"
                        type={"email"}
                        helperText="Email with admin permission"
                    />
                    {/* errors will return when field validation fails  */}
                    {errors.exampleRequired && <span>This field is required</span>}
                    <TextField
                        {...register("exampleRequired",
                            { required: true })}
                        label="User Token"
                        type={"password"}
                        helperText="User token"
                    />
                    {/* errors will return when field validation fails  */}
                    {errors.exampleRequired && <span>This field is required</span>}

                    <Button variant="contained" onSubmit={handleSubmit(onSubmit)} type="submit">Save</Button>
                    {/*</form >*/}
                    <Divider />
                    <TableContainer>
                        <Table sx={{minWidth: 600 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell  align="center"> Host </TableCell>
                                    <TableCell  align="center" > User </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {/* eslint-disable-next-line array-callback-return */}
                                {rows.map((row) => (
                                    <TableRow key={row.host}>
                                        <TableCell component="th" scope="row">
                                            {row.host}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {row.user}
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </FormControl>

        </>

    );
}
export default  ConnectForm;
